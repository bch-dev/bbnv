import { BchNetworkTypes, BchNetworkDetails } from "./types";
import { _bbnvConfig } from "./env";

const BITBOXSDK = require('bitbox-sdk').BITBOX;


const createBitboxMainnet = (): BchNetworkDetails => {
  return {
    BBSdk: new BITBOXSDK({
      restURL: "https://rest.bitcoin.com/v2/",
      bitdbURL: "https://bitdb.bitcoin.com/",
    }),
    NetworkType: "mainnet",
    NetName: "bitcoincash",
    ExplorerUrlBase: "https://explorer.bitcoin.com/bch",
    BBSocket: undefined
  }
}

const createBitboxTestnet = (): BchNetworkDetails => {
  return {
    BBSdk: new BITBOXSDK({
      restURL: "https://trest.bitcoin.com/v2/",
      bitdbURL: "https://tbitdb.bitcoin.com/",
    }),
    NetworkType: "testnet",
    NetName: "bchtest",
    ExplorerUrlBase: "https://explorer.bitcoin.com/tbch",
    BBSocket: undefined
  }
}


export const getBbEnvForNetworkType = (networkType: BchNetworkTypes): BchNetworkDetails => {
  switch (networkType) {
    case "mainnet": {
      if (!_bbnvConfig.MainnetBbnv) {
        _bbnvConfig.MainnetBbnv = createBitboxMainnet();
      }
      return _bbnvConfig.MainnetBbnv;
    }

    case "testnet":
    default: {
      if (!_bbnvConfig.TestnetBbnv) {
        _bbnvConfig.TestnetBbnv = createBitboxTestnet();
      }
      return _bbnvConfig.TestnetBbnv;
    }
  }
}


export const getBitBoxForEnv = (): BchNetworkDetails => {
  const network = _bbnvConfig.NetworkTypeProvider();
  const bbEnv = getBbEnvForNetworkType(network);
  return bbEnv;
}


export const setBitBoxNetworkTypeProvider = (provider: () => BchNetworkTypes) => {
  _bbnvConfig.NetworkTypeProvider = provider;
}


export const initBbnv = (provider: () => BchNetworkTypes) => {
  setBitBoxNetworkTypeProvider(provider);
}