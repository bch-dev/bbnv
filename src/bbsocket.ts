import { getBbEnvForNetworkType, getBitBoxForEnv } from "./bbnv";
import { BchNetworkTypes } from "./types";

import { _bbnvConfig } from './env';


export const getBitBoxSocketForNetworkType = (networkType: BchNetworkTypes) => {
  const bbEnv = getBbEnvForNetworkType(networkType);

  if (!bbEnv.BBSocket) {
    let bitboxSocketUrl = "";

    switch (networkType) {
      case "mainnet": {
        bitboxSocketUrl = "https://rest.bitcoin.com";
        break;
      }
      case "testnet": {
        bitboxSocketUrl = "https://trest.bitcoin.com";
        break;
      }
      default:
        throw new Error("Invalid network type: " + networkType)
    }
    
    const bitboxSocket = new bbEnv.BBSdk.Socket({
      restURL: bitboxSocketUrl
    });
    bbEnv.BBSocket = bitboxSocket;
  }

  return bbEnv.BBSocket;
}


export const getBitBoxSocketForEnv = () => {
  const network = _bbnvConfig.NetworkTypeProvider();
  return getBitBoxSocketForNetworkType(network);
}