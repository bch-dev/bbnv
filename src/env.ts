import { BchNetworkTypes, BbnvConfig } from "./types";


const _hostnameProvider = () => {
  return window.location.hostname;
}

const _localStorageProvider = (): Storage => {
  return window.localStorage;
}


const getNetworkTypeThrowUninitializedException = (): BchNetworkTypes => {
  throw new Error("No method to determine BCH network (mainnet or testnet) has been set. Call initBbnv() before calling getBitBoxForEnv().");
}


export const getNetworkTypeForWindowLocation = (hostnameProvider=_hostnameProvider): BchNetworkTypes => {

  const hostname = hostnameProvider();

  if (hostname === "localhost" ||
      hostname.includes("demo") ||
      hostname.includes("dev")) 
  {
    return "testnet";
  }

  return "mainnet";
}


export const getNetworkTypeFromLocalStorage = (
    localStorageProvider: () => Storage=_localStorageProvider, 
    missingStorageProvider: () => BchNetworkTypes=getNetworkTypeForWindowLocation): BchNetworkTypes => {

  const localStorage = localStorageProvider();

  const bbnvConfigStr = localStorage.getItem("bbnv-Config");

  if (bbnvConfigStr) {
    try {
      const bbnvConfig = JSON.parse(bbnvConfigStr);
      const networkType = bbnvConfig.networkType;
      if (networkType === "mainnet") {
        return "mainnet";
      } else {
        return "testnet";
      }
    } catch (e) {
      console.log(e);
    }
  }

  return missingStorageProvider();
}


export const useMainnetNetwork = (): BchNetworkTypes => "mainnet"

export const useTestnetNetwork = (): BchNetworkTypes => "testnet"


export const _bbnvConfig: BbnvConfig = {
  NetworkTypeProvider: getNetworkTypeThrowUninitializedException,
  MainnetBbnv: undefined,
  TestnetBbnv: undefined
}
