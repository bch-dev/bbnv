
export type HexStr = string;
export type TxId = HexStr;

export type WIFKey = string;
export type Address = string;

export type Utxo = {
  txid: TxId
  vout: number
  satoshis: number
}

export type Payment = {
  address: Address
  amount: number
}

export interface TxMinerFeeEstimate {
  minerFee: number
  changeAmount: number
  willReturnChange: boolean
}

export interface OpChopTxParameters {
  paymentKeyWif: string
  opReturnData?: Buffer
  changeAddress?: Address
  extraOutputs?: Payment[]
  utxos?: Utxo[]
}

export interface OpChopTxOutput {
  outputType: "payment" | "opReturn"
  outputPayment?: Payment
  outputOpReturn?: Buffer
}

export interface OpChopTxPlan {
  paymentKeyWif: string
  utxoInputs: Utxo[]
  outputs: OpChopTxOutput[]
  expectedMinerFee: number
}


const isHexStr = (hexStr: string): hexStr is HexStr => {
  var a = parseInt(hexStr,16);
  return a.toString(16) === hexStr.toLowerCase();
}


const isTxId = (txid: string): txid is TxId => {
  return txid.length === 64;
}


export const fromHexToByte = (twoCharHex: string): number => {
  const hexBytes = Buffer.from(twoCharHex, "hex");
  const theByte = hexBytes[0];
  return theByte;
}


export const byteToBuffer = (v: number): Buffer => {
  if (v < 0 || v > 255) {
    throw Error("Invalid byte value: " + v);
  }

  var a = [v];
  return Buffer.from(a);
}


export const txIdHexToBuffer = (txidHex: TxId) => {
  if (txidHex.length !== 64) {
    throw Error("Txid hex length is not 64 characters: " + txidHex);
  }

  return Buffer.from(txidHex, "hex")
}
