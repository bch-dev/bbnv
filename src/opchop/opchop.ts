import { fromHexToByte } from ".";

const OP_PUSHDATA1 = 76;
export const PUSH_1_BYTE = 0x01;
export const PUSH_32_BYTES = 0x20;


export interface OpField {
  readonly byteCount: number
  readonly hex: string
}

interface ProcessedChunk {
  readonly nextProcessingIndex: number,
  readonly opField: OpField,
}

const isSingleSizeByte = (sizeOp: number) => {
  if (sizeOp >= 1 && sizeOp <= 75) {
    return true;
  }
  return false;
}

const processNextChunk = (hex: string, processingIndex: number): ProcessedChunk | undefined => {
  const sizeOpHex = hex.slice(processingIndex, processingIndex + 2);
  const sizeOp = fromHexToByte(sizeOpHex);

  if (isSingleSizeByte(sizeOp)) {
    const nextProcessingIndex = processingIndex + 2 + (2 * sizeOp);
    const opHex = hex.slice(processingIndex + 2, nextProcessingIndex);

    return { 
      opField: {
        byteCount: sizeOp,
        hex: opHex,
      },
      nextProcessingIndex,
    } as ProcessedChunk;

  } else {

    if (sizeOp === OP_PUSHDATA1) {
      const sizeByteHex = hex.slice(processingIndex + 2, processingIndex + 4);
      const sizeByte = fromHexToByte(sizeByteHex);

      const nextProcessingIndex = processingIndex + 4 + (2 * sizeByte);
      const opHex = hex.slice(processingIndex + 4, nextProcessingIndex);

      return { 
        opField: {
          byteCount: sizeOp,
          hex: opHex,
        },
        nextProcessingIndex,
      } as ProcessedChunk;
    }
  }

  return undefined;
}

export const parseOpReturn = (hex: string): OpField[] | undefined => {
  // const expectedPrefix = "6a04" + LOKAD_ID;
    const lHex = hex.toLowerCase();
  
    if (lHex.indexOf("6a") !== 0) { return undefined; }
    let hexProcessingIndex = 2;
  
    let doneParsing = false;
    const opFields: OpField[] = [];
  
    while (!doneParsing) {
      const processed = processNextChunk(lHex, hexProcessingIndex);
      if (!processed) { return undefined; }
      if (processed.nextProcessingIndex >= lHex.length) {
        doneParsing = true;
      }
      opFields.push(processed.opField);
      hexProcessingIndex = processed.nextProcessingIndex;
    }
  
    return opFields;
  }