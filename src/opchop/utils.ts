import { getBbEnvForNetworkType, BchNetworkTypes, BchNetworkDetails } from "..";
import { OpChopTxParameters, TxMinerFeeEstimate, OpChopTxPlan, OpChopTxOutput } from ".";
import { Utxo, HexStr, TxId } from ".";


const getAnyBbnv = () => {
  return getBbEnvForNetworkType("testnet");
}

const getAnyBitbox = () => {
  return getAnyBbnv().BBSdk;
}

const getBbnvForPaymentKey = (paymentKey: string): BchNetworkDetails => {
  const networkType = getNetworkTypeForPaymentKey(paymentKey);
  return getBbEnvForNetworkType(networkType);
}

const getBbnvForAddress = (address: string): BchNetworkDetails => {
  const networkType = getNetworkTypeForAddress(address);
  return getBbEnvForNetworkType(networkType);
}


export const DUST_LIMIT = 546;


const sleep = (ms: number) => new Promise(resolve => setTimeout(resolve, ms))


export const privateKeyToCashAddress = (paymentKeyWif: string): string => {
  const bitbox = getAnyBitbox();

  let ecpair = bitbox.ECPair.fromWIF(paymentKeyWif);
  const appAddress = bitbox.ECPair.toCashAddress(ecpair);

  return appAddress;
}


export const findAllUtxos = async (senderAddress: string, retries: number = 10): Promise<Utxo[]> => {
  const bbnv = getBbnvForAddress(senderAddress);
  const bitbox = bbnv.BBSdk;
  const networkType = bbnv.NetworkType;

  const addressNetwork = bitbox.Address.detectAddressNetwork(senderAddress);
  if (addressNetwork !== networkType) {
    return [];
  }

  try {
    let count = 0;
    let details = await bitbox.Address.utxo(senderAddress);

    while (details === undefined) {
      await sleep(250);
      details = await bitbox.Address.utxo(senderAddress);
      count++;
      if (count > retries) {
        throw new Error(`findBestUtxo failed after ${retries} attempts`)
      }
    }

    const nonZeroUtoxs = details.utxos.filter((utxo: Utxo) => utxo.satoshis > 0);
    return nonZeroUtoxs;

  } catch (error) {
    console.error(error)
    throw error;
  }
}


export const findBalance = async (senderAddress: string, retries: number = 10): Promise<number> => {
  const bbnv = getBbnvForAddress(senderAddress);
  const bb = bbnv.BBSdk;
  const networkType = bbnv.NetworkType;

  const addressNetwork = bb.Address.detectAddressNetwork(senderAddress);
  if (addressNetwork !== networkType) {
    return -1;
  }

  try {
    let utxos = await findAllUtxos(senderAddress, retries);

    if (utxos.length > 0) {
      let balance = 0;
      utxos.forEach((element: Utxo) => {
        balance += element.satoshis;
      });
  
      return balance;
    }

    // throw new Error("No valid UTXO for address " + senderAddress);
    
    return 0;

  } catch (error) {
    console.error(error)
    throw error;
  }
}


export const createPrivateKeyAndAddress = (seedNetwork: "mainnet" | "testnet") => {
  const bbnv = getBbEnvForNetworkType(seedNetwork);
  const bb = bbnv.BBSdk;

  const entropy = bb.Crypto.randomBytes(32);
  const mnemonic = bb.Mnemonic.fromEntropy(entropy);
  
  const rootSeed = bb.Mnemonic.toSeed(mnemonic);

  const hdNode = bb.HDNode.fromSeed(rootSeed, seedNetwork);
  const keypair = bb.HDNode.toKeyPair(hdNode);
  const bchCashAddress = bb.ECPair.toCashAddress(keypair);
  const bchPrivateKey = bb.ECPair.toWIF(keypair) as string;

  return {bchPrivateKey, bchCashAddress};
}


const calculateMinerFee = (numInputs: number, numOutputs: number, opReturnData?: Buffer): number => {
  const bitbox = getAnyBitbox();

  const txBytesCount = bitbox.BitcoinCash.getByteCount({ P2PKH: numInputs }, { P2PKH: numOutputs });
  const fixedMinersFee = 20;
  let minerFee: number = txBytesCount + fixedMinersFee;

  if (opReturnData) {
    minerFee += opReturnData.length
  }

  return minerFee;
}


export const estimateOpChopTxMinerFee = async (opChopTx: OpChopTxParameters): Promise<TxMinerFeeEstimate> => {

  let utxos = opChopTx.utxos;
  if (!utxos) {
    const address = privateKeyToCashAddress(opChopTx.paymentKeyWif);
    utxos = await findAllUtxos(address);
  }

  const numInputs = utxos.length;

  const utxosSatoshis = utxos.map(utxo => utxo.satoshis);
  const utxosTotalSatoshis = utxosSatoshis.reduce((s1, s2) => s1 + s2, 0);
  
  let numOutputs = 1; // assume change address
  let extraOutputAmounts = 0;

  if (opChopTx.extraOutputs) {
    numOutputs += opChopTx.extraOutputs.length;
    opChopTx.extraOutputs.forEach(output => {
      extraOutputAmounts += output.amount;
    });
  }
  
  let minerFee: number = calculateMinerFee(numInputs, numOutputs, opChopTx.opReturnData);

  let changeAmount = utxosTotalSatoshis - extraOutputAmounts - minerFee;
  let willReturnChange = true;

  if (changeAmount < DUST_LIMIT) {
    minerFee = calculateMinerFee(numInputs, numOutputs - 1, opChopTx.opReturnData);
    changeAmount = utxosTotalSatoshis - extraOutputAmounts - minerFee;
    if (changeAmount > 0) {
      console.log("CHANGE AMOUNT IS DUST... EXTRA CHANGE ADDED TO MINER FEE: " + changeAmount + " -> " + minerFee)
      minerFee += changeAmount;
    }
    willReturnChange = false;
  }

  return {
    minerFee: minerFee,
    willReturnChange: willReturnChange,
    changeAmount: changeAmount,
  };
}


export const buildOpChopMessagePlan = async (opChopTx: OpChopTxParameters): Promise<OpChopTxPlan> => {

  const myAddress = privateKeyToCashAddress(opChopTx.paymentKeyWif);

  let extraOutputs = opChopTx.extraOutputs;
  if (!extraOutputs) {
    extraOutputs = [];
  }

  let opChopTxToUse = opChopTx;

  let utxos = opChopTx.utxos;
  if (!utxos) {
    utxos = await findAllUtxos(myAddress);
    opChopTxToUse = {
      ...opChopTx,
      utxos: utxos
    }
  }

  const minerEstimate = await estimateOpChopTxMinerFee(opChopTxToUse);

  const outputs: OpChopTxOutput[] = [];

  if (opChopTxToUse.opReturnData) {
    outputs.push({
      outputType: "opReturn",
      outputOpReturn: opChopTx.opReturnData,
    });
  }

  let changeAddress = myAddress;
  if (opChopTxToUse.changeAddress) {
    changeAddress = opChopTxToUse.changeAddress;
  }

  if (minerEstimate.willReturnChange) {
    outputs.push({
      outputType: "payment",
      outputPayment: {
        address: changeAddress,
        amount: minerEstimate.changeAmount
      }
    })
  }

  extraOutputs.forEach(output => {
    outputs.push({
      outputType: "payment",
      outputPayment: {
        address: output.address,
        amount: output.amount
      }
    })
  });

  const plan: OpChopTxPlan = {
    paymentKeyWif: opChopTx.paymentKeyWif,
    expectedMinerFee: minerEstimate.minerFee,
    utxoInputs: opChopTxToUse.utxos!,
    outputs: outputs
  };

  return plan;
}


export const buildOpChopMessageFromPlan = (plan: OpChopTxPlan): Promise<HexStr> => {
  const bbnv = getBbnvForPaymentKey(plan.paymentKeyWif);
  const bb = bbnv.BBSdk;

  const transactionBuilder = new bb.TransactionBuilder(bbnv.NetworkType);

  plan.utxoInputs.forEach((utxo) => {
    transactionBuilder.addInput(utxo.txid, utxo.vout);
  });

  plan.outputs.forEach((output) => {
    if (output.outputType === "opReturn") {
      transactionBuilder.addOutput(output.outputOpReturn, 0);
    } else {
      transactionBuilder.addOutput(output.outputPayment!.address, output.outputPayment!.amount);
    }
  })

  const ecpair = bb.ECPair.fromWIF(plan.paymentKeyWif);

  const redeemScript: any = undefined;

  plan.utxoInputs.forEach((utxo, index) => {
    transactionBuilder.sign(index, ecpair, redeemScript, transactionBuilder.hashTypes.SIGHASH_ALL, utxo.satoshis);
  });

  const tx = transactionBuilder.build();
  const txHex = tx.toHex();

  return txHex;
}


export const sendOpChopMessage = async (opChopTx: OpChopTxParameters): Promise<TxId> => {
  
  const plan = await buildOpChopMessagePlan(opChopTx);
  console.log("OPCHOP TX PLAN")
  console.log(plan)
  const txHex = buildOpChopMessageFromPlan(plan);

  const bbnv = getBbnvForPaymentKey(opChopTx.paymentKeyWif);
  const bb = bbnv.BBSdk;

  const result = bb.RawTransactions.sendRawTransaction(txHex);

  return result;
}


export const getNetworkTypeForPaymentKey = (paymentKey: string): BchNetworkTypes => {
  const bb = getAnyBitbox();
  const ecp = bb.ECPair.fromWIF(paymentKey);
  const address = bb.ECPair.toCashAddress(ecp);

  const networkType = bb.Address.detectAddressNetwork(address) as BchNetworkTypes;
  return networkType;
}


export const getNetworkTypeForAddress = (address: string): BchNetworkTypes => {
  const bb = getAnyBitbox();
  const networkType = bb.Address.detectAddressNetwork(address) as BchNetworkTypes;
  return networkType;
}
