
export type BchNetworkTypes = "mainnet" | "testnet"

export type NetworkName = "bitcoincash" | "bchtest"

export interface BchNetworkDetails {
  BBSdk: any
  NetworkType: BchNetworkTypes
  NetName: NetworkName
  ExplorerUrlBase: string
  BBSocket: any
}

export interface BbnvConfig {
  NetworkTypeProvider: () => BchNetworkTypes,
  MainnetBbnv?: BchNetworkDetails,
  TestnetBbnv?: BchNetworkDetails,
}


export interface BbnvConfigData {
  networkType: string
}